from django_filters import rest_framework as filters
from rest_framework import permissions, status, viewsets
from rest_framework.response import Response

from .models import Link, TaggedLink
from .serializers import LinkSerializer, LinkTagSerializer


class LinkViewSet(viewsets.ModelViewSet):
    queryset = Link.objects.all()
    serializer_class = LinkSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = (
        "status",
        "link",
    )
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)


class LinkTagViewSet(viewsets.ModelViewSet):
    queryset = TaggedLink.objects.all().distinct("tag")
    serializer_class = LinkTagSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
