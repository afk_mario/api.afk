from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r"link", views.LinkViewSet)
router.register(r"linkTags", views.LinkTagViewSet)

urlpatterns = [path("", include(router.urls))]
