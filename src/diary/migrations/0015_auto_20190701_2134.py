# Generated by Django 2.1.7 on 2019-07-01 21:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('diary', '0014_auto_20190701_1855'),
    ]

    operations = [
        migrations.AlterField(
            model_name='logfeed',
            name='small_text',
            field=models.CharField(max_length=255),
        ),
    ]
