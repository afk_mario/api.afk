import os

import html2text
import misaka as m
from adminsortable.fields import SortableForeignKey
from adminsortable.models import SortableMixin
from django.db import models
from django.template.defaultfilters import slugify
from django.urls import reverse
from taggit.managers import TaggableManager
from taggit.models import TaggedItemBase

IMAGE_TYPE = (("mni", "Main Image"), ("gal", "Gallery"))

htmlConverter = html2text.HTML2Text()
htmlConverter.ignore_links = True
htmlConverter.ignore_images = True
htmlConverter.ul_item_mark = "-"
htmlConverter.inline_links = False
htmlConverter.ignore_emphasis = True


def upload_to_feed_cover(instance, filename):
    filename_base, filename_ext = os.path.splitext(filename)
    return f"logs/covers/{instance.slug}{filename_ext.lower()}"


def imageLocation(instance, filename):
    from django.utils.timezone import now

    filename_base, filename_ext = os.path.splitext(filename)
    timestamp = now().strftime("%Y%m%d%H%M%S")
    return f"images/{filename_base}{timestamp}{filename_ext}"


class TaggedPost(TaggedItemBase):
    content_object = models.ForeignKey("Post", on_delete=models.CASCADE)


class LogFeed(models.Model):
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    title = models.CharField(max_length=140)
    slug = models.SlugField(editable=False, max_length=300)
    image = models.ImageField(upload_to=upload_to_feed_cover)
    website = models.URLField()
    text = models.TextField()
    author = models.CharField(max_length=140)
    author_mail = models.EmailField(max_length=140)
    language = models.CharField(max_length=10)
    small_text = models.CharField(max_length=255)
    feedBurner = models.URLField(blank=True, null=True)
    includeTags = models.TextField(blank=True, null=True)
    excludeTags = models.TextField(blank=True, null=True)
    dateCreated = models.DateTimeField(auto_now_add=True)
    dateUpdated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["order", "-dateCreated"]

    def plain_text(self):
        html_text = m.html(self.text)
        plain = htmlConverter.handle(html_text)
        return plain

    def encoded_text(self):
        html_text = m.html(self.text)
        return html_text

    def feed(self):
        if self.feedBurner:
            return self.feedBurner
        else:
            return "https://api.ellugar.co%s" % reverse(
                "logs-feed-rss", kwargs={"feed_slug": self.slug}
            )

    def get_absolute_url(self):
        return self.website

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        if self.pk is None:
            saved_image = self.image
            super(LogFeed, self).save(*args, **kwargs)
            self.image = saved_image

        super(LogFeed, self).save(*args, **kwargs)


class Post(models.Model):
    publish = models.BooleanField(default=False)
    title = models.CharField(max_length=140)
    slug = models.SlugField(editable=False, max_length=300)
    small_text = models.CharField(max_length=255, blank=True, null=True)
    author = models.CharField(max_length=140, blank=True, null=True)
    author_url = models.URLField(max_length=240, blank=True, null=True)
    text = models.TextField()
    tags = TaggableManager(through=TaggedPost, blank=True)
    dateCreated = models.DateTimeField(auto_now_add=True)
    dateUpdated = models.DateTimeField(auto_now=True)
    toot = models.CharField(max_length=140, blank=True, null=True)

    class Meta:
        ordering = ["-dateCreated", "title"]

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

    @property
    def image(self):
        mainImage = Image.objects.filter(post=self, imgType="mni")[:1].get()
        if mainImage:
            return mainImage.image
        else:
            return "No Image"

    def gallery(self):
        images = Image.objects.filter(post=self, imgType="gal")
        return images

    def get_absolute_url(self):
        return f"https://ellugar.co/logs/{self.slug}"

    def plain_text(self):
        html_text = m.html(self.text)
        plain = htmlConverter.handle(html_text)
        return plain

    def encoded_text(self):
        html_text = m.html(self.text)
        return html_text


class Image(SortableMixin):
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    publish = models.BooleanField(default=False)
    post = SortableForeignKey("Post", on_delete=models.CASCADE)
    altText = models.CharField(max_length=140, blank=True, null=True)
    caption = models.CharField(max_length=140, blank=True, null=True)
    image = models.ImageField(upload_to=imageLocation)
    imgType = models.CharField(max_length=3, choices=IMAGE_TYPE, default="gal")
    dateCreated = models.DateField(auto_now_add=True)

    class Meta:
        ordering = ["order", "dateCreated"]

    def __str__(self):
        return self.image.url

    def save(self, *args, **kwargs):
        if not self.altText:
            self.altText = self.post.slug
        super(Image, self).save(*args, **kwargs)
