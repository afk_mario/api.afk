import graphene
from apiArlefreak.custom_graphql_serializers import FlatTags
from graphene import relay
from graphene_django import DjangoObjectType
from graphene_django.converter import convert_django_field
from graphene_django.filter import DjangoFilterConnectionField
from taggit.managers import TaggableManager

from .models import Image, Post


@convert_django_field.register(TaggableManager)
def convert_tag_field_to_string(field, registry=None):
    return graphene.Field(
        FlatTags, description=field.help_text, required=not field.null
    )


class PostType(DjangoObjectType):
    class Meta:
        model = Post


class PostNode(DjangoObjectType):
    image = graphene.String()

    class Meta:
        model = Post
        filter_fields = ["publish", "tags__name"]
        interfaces = (relay.Node,)

    def resolve_image(self, args):
        try:
            image = Image.objects.filter(post=self, imgType="mni")[:1].get().image.url
        except Image.DoesNotExist:
            image = None

        return image


class ImageType(DjangoObjectType):
    image = graphene.String()

    class Meta:
        model = Image

    def resolve_image(self, args):
        return self.image and self.image.url


class Query(graphene.ObjectType):
    all_posts = DjangoFilterConnectionField(PostNode)
    all_images = graphene.List(ImageType)

    post = relay.Node.Field(PostNode, _id=graphene.Int(), slug=graphene.String())

    def resolve_all_images(self, info):
        return Image.objects.all()

    def resolve_post(self, info, **kwargs):
        id = kwargs.get("id")
        slug = kwargs.get("slug")

        if id is not None:
            return Post.objects.get(pk=id)

        if slug is not None:
            return Post.objects.get(slug=slug)

        return None


schema = graphene.Schema(query=Query)
