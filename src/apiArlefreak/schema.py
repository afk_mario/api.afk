import diary.schema
import graphene
import ligoj.schema
import podcast.schema
import portfolio.schema


class Query(
    diary.schema.Query,
    podcast.schema.Query,
    portfolio.schema.Query,
    ligoj.schema.Query,
    graphene.ObjectType,
):
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    pass


schema = graphene.Schema(query=Query)
