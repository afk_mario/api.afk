# Generated by Django 2.1.7 on 2019-05-21 22:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('podcast', '0032_auto_20190521_2226'),
    ]

    operations = [
        migrations.AddField(
            model_name='episode',
            name='season',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
