from datetime import datetime

import requests

from adminsortable.admin import SortableAdmin
from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Episode, Podcast


def update_publish_date(modeladmin, request, queryset):
    for item in queryset:
        item.dateCreated = datetime.now()
        item.save()


def update_episode_type_full(modeladmin, request, queryset):
    for item in queryset:
        item.episode_type = "full"
        item.save()


def update_episode_type_trailer(modeladmin, request, queryset):
    for item in queryset:
        item.episode_type = "trailer"
        item.save()


def update_episode_type_bonus(modeladmin, request, queryset):
    for item in queryset:
        item.episode_type = "bonus"
        item.save()


def send_update_signal(modeladmin, request, queryset):
    url = "https://api.netlify.com/build_hooks/5c91869f468f3ea7dfbab15e"
    requests.post(url)


update_publish_date.short_description = "Update publish date"


class AdminImageMixin(object):
    @mark_safe
    def admin_image(self, obj):
        return f"""<img
                    src='{obj.image.url}'
                    style='height: 100px; width: auto; display: block'
            />"""

    admin_image.short_description = "Preview"


class ViewOnSiteMixin(object):
    @mark_safe
    def view_on_site(self, obj):
        return f"""<a
                    class='button'
                    href='{obj.get_absolute_url()}'>
                        view on site
                    </a>
                """

    view_on_site.short_description = "View on site"


@admin.register(Podcast)
class PodcastAdmin(SortableAdmin, AdminImageMixin, ViewOnSiteMixin):
    save_as = True
    fieldsets = (
        (None, {"fields": ("title", "image")}),
        ("Author", {"fields": ("author", "author_mail")}),
        ("Categories", {"fields": ("parent_category", "child_category")}),
        (None, {"fields": ("language", "tags")}),
        (
            "Links",
            {
                "fields": (
                    "website",
                    "episodesUrl",
                    "iTunesURL",
                    "googlePodcast",
                    "spotify",
                    "feedBurner",
                )
            },
        ),
        (
            "Description",
            {"fields": ("small_text", "episode_footer_template", "text", "faq")},
        ),
    )
    list_display = ["order", "dateCreated", "slug", "small_text", "admin_image"]
    list_display_links = ["order", "dateCreated", "slug", "small_text", "admin_image"]


@admin.register(Episode)
class EpisodeAdmin(SortableAdmin, AdminImageMixin, ViewOnSiteMixin):
    save_as = True
    fieldsets = (
        (
            None,
            {"fields": ("publish","podcast", "episode_type", "episode_number", "title", "image")},
        ),
        ("Files", {"fields": ("audio_mp3", "audio_ogg")}),
        ("File Info", {"fields": ("audio_type", "duration", "audio_size")}),
        ("Description", {"fields": ("small_text", "text")}),
    )
    list_display = [
        "publish",
        "slug",
        "title",
        "season",
        "episode_number",
        "episode_type",
        "podcast",
        "duration",
        "dateCreated",
        "small_text",
        "admin_image",
    ]
    list_display_links = [
        "slug",
        "title",
        "podcast",
        "duration",
        "dateCreated",
        "small_text",
        "admin_image",
    ]
    list_editable = ("publish", "season", "episode_number", "episode_type")
    list_filter = ("podcast", "episode_type")
    actions = [
        update_publish_date,
        update_episode_type_full,
        update_episode_type_trailer,
        update_episode_type_bonus,
        send_update_signal,
    ]
