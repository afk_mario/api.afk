import os

from adminsortable.fields import SortableForeignKey
from adminsortable.models import SortableMixin
from django.db import models
from django.template.defaultfilters import slugify
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill


def imageLocation(instance, filename):
    from django.utils.timezone import now

    filename_base, filename_ext = os.path.splitext(filename)
    timestamp = now().strftime("%Y%m%d%H%M%S")
    return f"images/{filename_base}{timestamp}{filename_ext}"


class Entry(SortableMixin):
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    publish = models.BooleanField(default=False)
    name = models.CharField(max_length=140)
    slug = models.SlugField(editable=False)
    text = models.TextField()
    dateCreated = models.DateField(auto_now_add=True)
    dateUpdated = models.DateField(auto_now=True)

    class Meta:
        ordering = ["order", "name"]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Entry, self).save(*args, **kwargs)


class Image(SortableMixin):
    order = models.PositiveIntegerField(default=0, editable=False, db_index=True)
    publish = models.BooleanField(default=False)
    entry = SortableForeignKey("Entry", on_delete=models.CASCADE)
    order_with_respect_to = "entry"
    name = models.CharField(max_length=140)
    caption = models.CharField(max_length=140, blank=True)
    image = models.ImageField(upload_to=imageLocation)
    dateCreated = models.DateField(auto_now_add=True)
    thumbnail = ImageSpecField(
        source="image",
        processors=[ResizeToFill(200, 200)],
        format="PNG",
        options={"quality": 100},
    )

    class Meta:
        ordering = ["order", "dateCreated"]

    def __str__(self):
        return self.name
