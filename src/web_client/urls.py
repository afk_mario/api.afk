# from .custom_routers import HybridRouter
from django.urls import include, path
from rest_framework import routers

from . import views

# router = HybridRouter()
# router.add_api_view(
#     'config',
#     url(r'^config/$', views.SiteConfigurationViewSet.as_view(),
#         name='site_configuration'))

router = routers.DefaultRouter()
router.register(r"^config", views.SiteConfigurationViewSet)

urlpatterns = [path("", include(router.urls))]
